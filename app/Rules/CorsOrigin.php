<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CorsOrigin implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $url_passes = filter_var($value, FILTER_VALIDATE_URL) !== false;

        $wildcard_passes = $value === '*';

        return $url_passes || $wildcard_passes;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.cors_origin');
    }
}
