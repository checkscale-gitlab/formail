<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class InfectedKeyword implements Rule
{

    /**
     * All laravel validation keywords which are forbidden because of security reasons.
     * @var array
     */
    protected array $forbiddenValidationTerms = [
        'unique', 'exists', 'password', 'active_url'
    ];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $lower = Str::lower($value);
        return Str::contains($lower, $this->forbiddenValidationTerms) === false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.infectedkeyword');
    }

    /**
     * @return array
     */
    public function getForbiddenValidationTerms(): array
    {
        return $this->forbiddenValidationTerms;
    }
}
