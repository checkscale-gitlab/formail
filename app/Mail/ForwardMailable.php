<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForwardMailable extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    private array $entries;
    /**
     * @var string
     */
    private string $viewName;

    /**
     * Create a new message instance.
     *
     * @param string $view
     * @param array $entries
     */
    public function __construct(string $view, array $entries)
    {
        $this->viewName = $view;
        $this->entries = $entries;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->viewName, $this->entries);
    }
}
