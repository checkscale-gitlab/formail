<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'admin', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function credentials()
    {
        return $this->hasMany(Credential::class)->latest('updated_at');
    }

    public function validations()
    {
        return $this->hasMany(Validation::class)->latest('updated_at');
    }

    public function receivers()
    {
        return $this->hasMany(Receiver::class)->latest('updated_at');
    }

    public function endpoints()
    {
        return $this->hasMany(Endpoint::class)->latest('updated_at');
    }

    public function entries()
    {
        return $this->hasMany(Entry::class)->latest('updated_at');
    }

}
