<?php

namespace App\Http\Controllers;

use App\Endpoint;
use App\Entry;
use App\Mail\ForwardMailable;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Swift_Mailer;


class ForwardController extends Controller
{
    public function preflight(string $path)
    {
        $endpoints = $this->getEndpoint($path);
        $this->checkExists($endpoints);
        /**
         * @var $endpoint Endpoint
         */
        $endpoint = $endpoints->first();
        return $this->appendHeaders($endpoint);
    }

    public function incoming(string $path)
    {
        $endpoints = $this->getEndpoint($path);
        $this->checkExists($endpoints);
        /**
         * @var $endpoint Endpoint
         */
        $endpoint = $endpoints->first();
        $this->checkMonthlyRateLimiting($endpoint);
        $this->checkClientRateLimiting($endpoint);

        $validator = $this->getValidationRules($endpoint->entries);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors(), 422);
        }

        $this->sendMail($endpoint, $validator->validated());
        return $this->appendHeaders($endpoint);
    }

    /**
     * @param $endpoints
     */
    protected function checkExists($endpoints): void
    {
        if (!$endpoints->exists()) {
            abort(404);
        }
    }

    private function checkMonthlyRateLimiting(Endpoint $endpoint)
    {
        if ($this->isMonthlyLimitExceeded($endpoint->path, $endpoint->monthly_limit)) {
            abort(429);
        }
    }

    private function isMonthlyLimitExceeded(string $key, int $limit)
    {
        $counter = date('Y:m') . $key;
        $count = Redis::incr($counter);
        Redis::expire($counter, 2629800);
        return $count > $limit;
    }

    /**
     * @param Endpoint $endpoint
     */
    private function checkClientRateLimiting(Endpoint $endpoint)
    {
        if ($this->isClientLimitExceeded($endpoint->path, $endpoint->client_limit, $endpoint->time_unit)) {
            abort(429);
        }
    }

    /**
     * @param string $key
     * @param int $client_limit
     * @param string $time_unit
     * @return bool
     */
    private function isClientLimitExceeded(string $key, int $client_limit, string $time_unit)
    {
        $expiration = $this->getExpiration($time_unit);
        $origin = $this->getOrigin();

        $counter = $origin . $key;
        $count = Redis::incr($counter);
        Redis::expire($counter, $expiration);
        return $count > $client_limit;
    }

    /**
     * @param string $time_unit
     * @return int
     */
    private function getExpiration(string $time_unit)
    {
        switch ($time_unit) {
            case 'month':
                return 2629800;
            case 'week':
                return 604800;
            case 'day':
                return 86400;
            case 'hour':
                return 3600;
            case 'minute':
                return 60;
            default:
                return 0;
        }
    }

    /**
     * @return string
     */
    private function getOrigin(): string
    {
        return Request::ip();
    }

    /**
     * @param Collection $entries
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function getValidationRules(Collection $entries)
    {
        $rules = $entries->flatMap(function (Entry $entry) {
            return [$entry->keyword => $entry->validation->validation];
        })->toArray();

        return Validator::make(request()->input(), $rules);
    }

    /**
     * return error response.
     *
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['errors'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    private function sendMail(Endpoint $endpoint, array $validatedInput)
    {
        $entries = $this->fetchOutput($endpoint, $validatedInput);
        /**
         * @var $mailer Mailer
         */
        $config = $endpoint->credential->toArray();
        $config['password'] = $endpoint->credential->password;
        $mailer = app()->makeWith(Swift_Mailer::class, $config);
        $receivers = $endpoint->receivers->map(fn($receiver) => $receiver->email)->toArray();
        $mailer->to($receivers)
            ->send(new ForwardMailable('emails.forward', [
                'entries' => $entries->toArray(),
                'endpoint' => $endpoint
            ]));
    }

    /**
     * @param string $path
     * @return mixed
     */
    private function getEndpoint(string $path)
    {
        return Endpoint::where(['path' => $path]);
    }

    /**
     * @param Endpoint $endpoint
     * @return ResponseFactory|Response
     */
    private function appendHeaders(Endpoint $endpoint)
    {
        return response('', 200, [
            'Access-Control-Allow-Origin' => $endpoint->cors_origin,
            'Access-Control-Allow-Methods' => 'POST, OPTIONS',
            'Access-Control-Allow-Headers' => 'Content-Type, Origin'
        ]);
    }

    private function fetchOutput(Endpoint $endpoint, array $validatedInput)
    {
        $result = new Collection();
        foreach ($validatedInput as $key => $value) {
            $result->push([
                'name' => $endpoint->entries()->where(['keyword' => $key])->first()->name,
                'keyword' => $key,
                'value' => $value
            ]);
        }
        return $result;
    }

}
