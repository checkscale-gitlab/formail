<?php

namespace Tests\Unit;


use App\Endpoint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateRandomEndpointString extends TestCase
{

    use RefreshDatabase;

    /**
     * A random string should be created when creating new Endpoint
     *
     * @return void
     */
    public function testRandomStringIsGenerated()
    {
        $endpoint = factory(Endpoint::class)->create();
        $this->assertTrue(isset($endpoint->path));

        $this->assertDatabaseHas('endpoints', $endpoint->toArray());

        $this->assertIsString(Endpoint::where([
            'id' => $endpoint->id
        ])->first()->path);
    }
}
