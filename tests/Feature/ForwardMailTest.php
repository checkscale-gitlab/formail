<?php

namespace Tests\Feature;

use App\Endpoint;
use Facades\Tests\Setup\EndpointFactory;
use Illuminate\Foundation\Testing\Concerns\InteractsWithRedis;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Mail\PendingMail;
use Illuminate\Support\Facades\Redis;
use Mockery\MockInterface;
use Swift_Mailer;
use Tests\TestCase;

class ForwardMailTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    use RefreshDatabase, InteractsWithRedis;

    /**
     *
     *
     * @return void
     */
    public function testCheckIfPathExists()
    {
        $this->withoutExceptionHandling();
        $this->mockEmail(1);
        $endpoint = factory(Endpoint::class)->create();
        $this->mockRedisShouldPass($endpoint);
        $this->post('/api/formail/' . $endpoint->path)
            ->assertOk();
    }

    public function testPathNotFound()
    {
        $this->post('/api/formail/NOTFOUND')
            ->assertNotFound();
    }

    public function testRateLimitingMonthly()
    {
        $endpoint = factory(Endpoint::class)->create([
            'monthly_limit' => 1,
            'client_limit' => 1000,
            'time_unit' => 'month'
        ]);

        $limit = $endpoint->monthly_limit + 1;
        $this->mockRedisMonthly($endpoint, $limit);

        $this->post('/api/formail/' . $endpoint->path)
            ->assertStatus(429);
    }

    public function testClientLimit()
    {
        $endpoint = factory(Endpoint::class)->create([
            'monthly_limit' => 100,
            'client_limit' => 1,
            'time_unit' => 'hour'
        ]);

        $limit = $endpoint->client_limit + 1;
        $this->mockRedisClient($limit);

        $this->post('/api/formail/' . $endpoint->path)
            ->assertStatus(429);
    }

    public function testInputAndValidation()
    {
        $this->withoutExceptionHandling();
        $endpoint = EndpointFactory::withEntries(2)->create();

        $this->mockRedisShouldPass($endpoint);

        $keywords = $endpoint->entries->map(fn($entry) => $entry->keyword);

        $data = [];
        foreach ($keywords as $keyword) {
            $data[$keyword] = -1;
        }

        $this->post('/api/formail/' . $endpoint->path, $data)
            ->assertJsonValidationErrors($keywords->toArray())
            ->assertStatus(422);
    }

    /**
     * @param int $times
     * @return MockInterface
     */
    private function mockEmail(int $times): MockInterface
    {
        $mockMailer = $this->mock(Swift_Mailer::class, function ($mock) use ($times) {
            $mock->shouldReceive('to')->times($times)->andReturn($this->mock(PendingMail::class, function ($mock) use ($times) {
                $mock->shouldReceive('send')->times($times);
            }));
        });
        app()->bind(Swift_Mailer::class, function () use ($mockMailer) {
            return $mockMailer;
        });
        return $mockMailer;
    }

    public function testPreflightRequest()
    {
        $endpoint = factory(Endpoint::class)->create();
        $this->options('/api/formail/' . $endpoint->path)
            ->assertHeader('Access-Control-Allow-Origin', $endpoint->cors_origin)
            ->assertHeader('Access-Control-Allow-Methods', 'POST, OPTIONS')
            ->assertHeader('Access-Control-Allow-Headers', 'Content-Type, Origin')
            ->assertOk();
    }

    public function testPreflightRequestNotFound()
    {
        $this->options('/api/formail/INVALID')
            ->assertNotFound();
    }

    public function testCORSHeaders()
    {
        $this->mockEmail(1);
        $endpoint = factory(Endpoint::class)->create();
        $this->mockRedisShouldPass($endpoint);
        $this->post('/api/formail/' . $endpoint->path)
            ->assertHeader('Access-Control-Allow-Origin', $endpoint->cors_origin)
            ->assertHeader('Access-Control-Allow-Methods', 'POST, OPTIONS')
            ->assertHeader('Access-Control-Allow-Headers', 'Content-Type, Origin')
            ->assertOk();
    }

    /**
     * @param $endpoint
     * @param $limit
     */
    private function mockRedisMonthly($endpoint, $limit): void
    {
        $redisKey = date('Y:m') . $endpoint->path;
        Redis::shouldReceive('incr')
            ->once()
            ->andReturn($limit);
        Redis::shouldReceive('expire')
            ->once();
    }

    /**
     * @param $limit
     */
    private function mockRedisClient($limit): void
    {
        Redis::shouldReceive('incr')
            ->twice()
            ->andReturn($limit);
        Redis::shouldReceive('expire')
            ->twice();
    }

    /**
     * @param $endpoint
     */
    private function mockRedisShouldPass($endpoint): void
    {
        if ($endpoint->client_limit < $endpoint->monthly_limit) {
            $limit = $endpoint->client_limit - 1;
        } else {
            $limit = $endpoint->monthly_limit - 1;
        }
        Redis::shouldReceive('incr')
            ->twice()
            ->andReturn($limit);
        Redis::shouldReceive('expire')
            ->twice();
    }
}
