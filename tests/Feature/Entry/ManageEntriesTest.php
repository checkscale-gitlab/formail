<?php

namespace Tests\Feature\Entry;

use App\Entry;
use App\Validation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ManageEntriesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can create entry types.
     *
     * @return void
     */
    public function testUserCanCreateEntries()
    {
        $this->withoutExceptionHandling();

        $validation = factory(Validation::class)->create();

        $this->actingAs($validation->user)
            ->get('/entries/create')
            ->assertOk();

        $entry = [
            'name' => 'name',
            'keyword' => 'data_name',
            'validation_id' => $validation->id
        ];

        $response = $this->actingAs($validation->user)
            ->post('/entries', $entry);

        $entry['user_id'] = $validation->user->id;

        $this->assertDatabaseHas('entries', $entry);

        $response->assertRedirect('/entries/' . Entry::where($entry)->first()->id);

        $this->get('/entries')
            ->assertSee($entry['name'])
            ->assertSee($entry['keyword']);
    }

    /**
     * The entry has to be validated
     */
    public function testCreateEntryRulesAreValidated()
    {
        $this->signIn();

        $entries = [
            'name' => '',
            'keyword' => 'invalid keyword',
            'validation_id' => 999
        ];

        $response = $this->post('/entries', $entries);
        $response->assertSessionHasErrors([
            'name', 'keyword', 'validation_id'
        ]);

        $this->assertDatabaseMissing('entries', $entries);
    }

    /**
     * A user can view his own entries in detail
     */
    public function testUserCanViewAEntry()
    {
        $this->withoutExceptionHandling();
        $entry = factory(Entry::class)->create();

        $this->actingAs($entry->user)
            ->get('/entries/' . $entry->id)
            ->assertOk()
            ->assertSee($entry->name)
            ->assertSee($entry->entry);
    }


    public function testGuestsCannotManageEntries()
    {
        $this->get('/entries')
            ->assertRedirect('/login');

        $this->get('/entries/create')
            ->assertRedirect('/login');

        $this->get('/entries/1')
            ->assertRedirect('/login');

        $this->delete('/entries/1')
            ->assertRedirect('/login');

        $this->patch('/entries/1')
            ->assertRedirect('/login');

        $this->post('/entries')
            ->assertRedirect('/login');
    }

    /**
     * A user can only view his own entries
     */
    public function testUsersCannotViewOthersEntries()
    {
        $this->signIn();
        $ownEntry = factory(Entry::class)->create();
        $entry = factory(Entry::class)->create();
        $this->get('/entries/' . $entry->id)
            ->assertStatus(403)
            ->assertDontSee($entry->name);

        $this->actingAs($ownEntry->user)
            ->get('/entries')
            ->assertSee($ownEntry->name)
            ->assertDontSee($entry->name);
    }

    /**
     * A user can delete his entries
     */
    public function testUserCanDeleteHisEntries()
    {
        $this->withoutExceptionHandling();
        $entry = factory(Entry::class)->create();

        $this->actingAs($entry->user)
            ->delete('/entries/' . $entry->id)
            ->assertRedirect('/entries');

        $this->assertDatabaseMissing('entries', [
            'id' => $entry->id
        ]);
    }


    /**
     * A user cannot delete others entries
     */
    public function testUserCannotDeleteOthersEntries()
    {
        $this->signIn();
        $entry = factory(Entry::class)->create();

        $this
            ->delete('/entries/' . $entry->id)
            ->assertStatus(403);

        $this->assertDatabaseHas('entries', [
            'id' => $entry->id
        ]);
    }

    public function testUserCanUpdateHisEntries()
    {
        $this->withoutExceptionHandling();
        $entry = factory(Entry::class)->create();

        $changedEntry = [
            'name' => 'name',
            'keyword' => 'changed_keyword',
            'validation_id' => factory(Validation::class)->create([
                'user_id' => $entry->user
            ])->id
        ];

        $path = '/entries/' . $entry->id;
        $this->actingAs($entry->user)
            ->patch($path, $changedEntry)
            ->assertRedirect($path);

        $this->assertDatabaseHas('entries', $changedEntry);
    }

    public function testUpdateEntriesIsValidated()
    {
        $entry = factory(Entry::class)->create();

        $changedEntry = [
            'name' => '',
            'keyword' => '9invalid$keyword',
            'validation_id' => factory(Validation::class)->create()->id
        ];

        $path = '/entries/' . $entry->id;
        $this->actingAs($entry->user)
            ->patch($path, $changedEntry)
            ->assertSessionHasErrors([
                'name', 'keyword', 'validation_id'
            ]);

        $this->assertDatabaseHas('entries', [
            'name' => $entry->name,
            'keyword' => $entry->keyword,
            'validation_id' => $entry->validation_id
        ]);
        $this->assertDatabaseMissing('entries', $changedEntry);
    }


    /**
     * A user cannot update others credentials
     */
    public function testUserCannotUpdateOthersEntries()
    {
        $this->signIn();
        $entry = factory(Entry::class)->create();

        $changedEntry = [
            'name' => 'changed'
        ];
        $this
            ->patch('/entries/' . $entry->id, $changedEntry)
            ->assertStatus(403);

        $this->assertDatabaseHas('entries', $entry->toArray());
        $this->assertDatabaseMissing('entries', $changedEntry);
    }

    public function testUsersCannotUseOthersValidations()
    {
        $this->signIn();

        $validation = factory(Validation::class)->create();

        $endpoint = [
            'validation_id' => $validation->id
        ];

        $this->post('/entries', $endpoint)
            ->assertSessionHasErrors('validation_id');
    }

}
