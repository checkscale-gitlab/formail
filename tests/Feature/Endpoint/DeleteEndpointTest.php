<?php

namespace Tests\Feature\Endpoint;

use App\Endpoint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteEndpointTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can delete his endpoints
     */
    public function testUserCanDeleteHisEndpoints()
    {
        $this->withoutExceptionHandling();
        $endpoint = factory(Endpoint::class)->create();

        $this->actingAs($endpoint->user)
            ->delete('/endpoints/' . $endpoint->id)
            ->assertRedirect('/endpoints');

        $this->assertDatabaseMissing('endpoints', [
            'id' => $endpoint->id
        ]);
    }


    /**
     * A user cannot delete others endpoints
     */
    public function testUserCannotDeleteOthersEndpoints()
    {
        $this->signIn();
        $endpoint = factory(Endpoint::class)->create();

        $this
            ->delete('/endpoints/' . $endpoint->id)
            ->assertStatus(403);

        $this->assertDatabaseHas('endpoints', [
            'id' => $endpoint->id
        ]);
    }
}
