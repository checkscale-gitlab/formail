<?php

/** @var Factory $factory */

use App\Entry;
use App\User;
use App\Validation;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Entry::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'keyword' => $faker->word,
        'user_id' => factory(User::class)->create()->id,
        'validation_id' => function (array $entry) {
            return factory(Validation::class)->create([
                'user_id' => $entry['user_id']
            ]);
        }
    ];
});
