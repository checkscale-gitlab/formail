<?php

/** @var Factory $factory */

use App\Credential;
use App\Endpoint;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Endpoint::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'cors_origin' => $faker->url,
        'subject' => $faker->sentence,
        'monthly_limit' => $faker->numberBetween(),
        'client_limit' => $faker->numberBetween(),
        'time_unit' => $faker->randomElement([
            'month', 'week', 'day', 'hour', 'minute'
        ]),
        'user_id' => factory(User::class)->create(),
        'credential_id' => function (array $endpoint) {
            return factory(Credential::class)->create([
                'user_id' => $endpoint['user_id']
            ]);
        },
        'path' => $faker->unique()->randomLetter
    ];
});
