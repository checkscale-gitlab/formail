@extends('layouts.app')

@section('content')
    <div style="margin-top: -24px;">
        <div class="jumbotron">
            <h1 class="display-4 text-center">Formail</h1>
            <p class="lead text-center">API-Backend to validate and forward form data from client-side only
                webappications via
                email.</p>
            <hr class="my-4">
            <div class="d-flex flex-wrap mb-0 justify-content-center">
                @if (Route::has('login'))
                    @auth
                        <a class="btn btn-primary btn-lg m-1" href="{{ url('/home') }}" role="button">Home</a>
                    @else
                        <a class="btn btn-primary btn-lg m-1" href="{{ route('login') }}" role="button">Login</a>
                        @if (Route::has('register'))
                            <a class="btn btn-primary btn-lg m-1" href="{{ route('register') }}"
                               role="button">Register</a>
                        @endif
                    @endauth
                @endif
                <a class="btn btn-primary btn-lg m-1" href="https://docs.formail.dev" role="button" target="_blank">Documentation</a>
                <a class="btn btn-primary btn-lg m-1" href="https://gitlab.com/aakado/formail" role="button"
                   target="_blank">Source Code</a>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center d-flex">
            <img src="{{asset('images/svg/undraw_fill_forms_yltj.svg')}}" alt="" width="256" height="256"
                 title="Fill Forms" class="col-md mb-4 align-self-center">
            <div class="col-md align-self-center">
                <h2>Forms on client-side only websites?</h2>
                <h4>Are you designing a client-side only website, but want to provide forms for the users?</h4>
                <br>
                <h4>Formail makes this possible without the need to deploy a backend.</h4>
            </div>
        </div>
        <hr class="my-4">
        <div class="row justify-content-center">
            <div class="col-md align-self-center mb-4">
                <h2>Don't want to care about a backend?</h2>
                <h4>Formail is the backend for your forms. It receives, validates and forwards all requests into your
                    desired inbox. Just send the form data with a HTTP POST request to one of your endpoints.</h4>
            </div>
            <img src="{{asset('images/svg/undraw_server_q2pb.svg')}}" alt="" width="256" height="256"
                 title="Fill Forms" class="col-md mb-4 align-self-center">
        </div>
        <hr class="my-4">
        <div class="row justify-content-center">
            <img src="{{asset('images/svg/undraw_security_o890.svg')}}" alt="" width="256" height="256"
                 title="Fill Forms" class="col-md mb-4 align-self-center">
            <div class="col-md align-self-center mb-4">
                <h2>What about security?</h2>
                <h4>Formail is built with security in mind:</h4>
                <ul>
                    <li>Built with Laravel, which includes protection against common security risks</li>
                    <li>Only accessible via https</li>
                    <li>Configure Access-Control-Allow-Origin Headers</li>
                    <li>Only encrypted SMTP connections allowed</li>
                    <li>Protect your endpoints with monthly- and a per-client limits</li>
                    <li>Sensitive user data is stored encrypted</li>
                    <li>Create validation rules for the form data</li>
                </ul>
            </div>
        </div>
        <hr class="my-4">
        <div class="row justify-content-center">
            <div class="col-md align-self-center mb-4">
                <h2>Regain focus on designing websites</h2>
                @if (Route::has('login'))
                    @auth
                        <h4>Create an endpoint to get started.</h4>
                        <a class="btn btn-primary mt-2" href="{{ url('/endpoints/create') }}" role="button">Create
                            Endpoint</a>
                    @else
                        <h4>Register or log in now and create your first endpoint to get started.</h4>
                        <div class="mt-2 justify-content-center d-flex">
                            <a class="btn btn-primary mx-1" href="{{ route('login') }}"
                               role="button">Login</a>
                            @if (Route::has('register'))
                                <a class="btn btn-primary mx-1" href="{{ route('register') }}"
                                   role="button">Register</a>
                            @endif
                        </div>
                    @endauth
                @endif
            </div>
            <img src="{{asset('images/svg/undraw_dev_focus_b9xo.svg')}}" alt="" width="256" height="256"
                 title="Fill Forms" class="col-md mb-4 align-self-center">
        </div>
    </div>
@endsection
