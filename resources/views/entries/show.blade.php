@extends('layouts.app')

@section('content')
    @include('entries._header')
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <form action="/entries/{{$entry->id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    @include('entries._form')
                    <div class="mt-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="/entries">
                            <button type="button" class="btn btn-secondary">Back</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection
