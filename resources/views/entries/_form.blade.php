<div class="form-row">
    @include('form._form-element', [
'field'=>'name',
'text' => 'Name',
'placeholder'=>'Name',
'value'=> $entry->name,
'description'=> 'The name of the entry',
'error_message'=> 'Name was invalid.',
'additional_options'=>'required'
])
</div>
<div class="form-row">
    @include('form._form-element', [
'field'=>'keyword',
'text' => 'Keyword',
'placeholder'=>'Keyword',
'value'=> $entry->keyword,
'description'=> 'The keyword of the json attribute',
'error_message'=> 'Keyword was invalid.',
'additional_options'=>'required'
])
</div>
<div class="form-row">
    <div class="col">
        <label for="validation_id">Validation</label>
        <select class="custom-select" name="validation_id" id="validation_id" required>
            @foreach($validations as $validation)
                <option value="{{$validation->id}}"
                        @if($validation->id === $entry->validation_id)selected="selected"
                        @endif aria-describedby="validation_idHelp">{{$validation->name}}</option>
            @endforeach
        </select>
        <small id="validation_idHelp" class="form-text text-muted">The validation with which the entry will be
            validated</small>
        <div class="invalid-feedback">Validation was invalid.</div>
    </div>
</div>
