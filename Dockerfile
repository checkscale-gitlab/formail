FROM php:7.4-fpm-alpine3.11
RUN apk update \
    && apk add --no-cache --virtual .build-deps \
    ${PHPIZE_DEPS} \
    postgresql-dev \
    && apk add --no-cache --virtual .dev-deps \
    composer \
    && apk --no-cache add \
    php7-openssl \
    php7-bcmath \
    php7-ctype \
    php7-json \
    php7-mbstring \
    php7-tokenizer \
    php7-xml \
    libpq \
    php7-opcache \
    && pecl install -o -f redis \
    && docker-php-ext-install \
    pdo_pgsql \
    && docker-php-ext-enable redis \
    && apk del -q --purge .build-deps

WORKDIR /app
RUN chown -R www-data:www-data .
USER www-data

COPY --chown=www-data:www-data . .

ENV PATH /app/vendor/bin:$PATH
RUN composer global require hirak/prestissimo --no-interaction --no-progress --no-suggest \
    && composer install --no-interaction --no-progress --no-suggest \
    && composer clearcache

EXPOSE 9000
ENTRYPOINT ["ash", "entrypoint.sh"]
